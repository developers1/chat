<?php
namespace App\Interfaces;


interface UserRepositoryInterface
{
    public function getAllUsers();
    public function findById($id);
}