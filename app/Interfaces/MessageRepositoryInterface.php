<?php
namespace App\Interfaces;


interface MessageRepositoryInterface
{
    public function getById($from, $to);
    public function storeMessage($from, $to, $message_txt );

}