<?php

namespace App\Repositories;

use App\Interfaces\MessageRepositoryInterface;
use App\Message;

class MessageRepository implements MessageRepositoryInterface
{
    protected $message;
    public function __construct(Message $message)
    {
        $this->message=$message;
    }

    public function getById($from, $to)
    {
        return $this->message->with(['from', 'to'])->where(function($query) use($from, $to) {
            $query->whereFromId($from)->whereToId($to);
        })->orWhere(function($query) use($from, $to) {
            $query->whereToId($from)->whereFromId($to);
        })->get();;
    }
    public function storeMessage($from, $to, $message_txt)
    {
        $this->message->from()->associate($from);
        $this->message->to()->associate($to);
        $this->message->message = $message_txt;
        $this->message->save();
        return $this->message;
    }
}