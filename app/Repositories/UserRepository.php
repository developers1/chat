<?php

namespace App\Repositories;



use App\Interfaces\UserRepositoryInterface;
use App\User;

class UserRepository implements UserRepositoryInterface
{
    protected $user;
    public function __construct(User $user)
    {
        $this->user=$user;
    }

    public function getAllUsers()
    {
        return $this->user->get();
    }
    public function findById($id)
    {
        return $this->user->find($id);
    }
}