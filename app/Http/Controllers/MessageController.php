<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Interfaces\MessageRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    protected $messageRepository;
    protected $userRepository;
    public function __construct(MessageRepositoryInterface $messageRepository,UserRepositoryInterface $userRepository)
    {
        $this->messageRepository=$messageRepository;
        $this->userRepository=$userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from = Auth::user()->id;
        $to = $request->to;
        return $this->messageRepository->getById($from, $to);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from = Auth::user();
        $to =$this->userRepository->findById($request->to);
        $message_txt = $request->message;
        $message_result=$this->messageRepository->storeMessage($from,$to,$message_txt);
        event(new NewMessage($message_result));
    }
}
