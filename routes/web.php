<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    $data = [
        'event' => 'UserSignedUp',
        'data' => [
            'username' => 'JohnDoe'
        ]
    ];
    Redis::publish('test-channel', json_encode($data));
    return view('welcome');
});
Route::post('/chat', 'MessageController@store');
Route::get('/messages', 'MessageController@index');

Route::get('/users', 'UserController@index');
Auth::routes();

Route::get('/home', 'HomeController@index');
